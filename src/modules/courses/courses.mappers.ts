import { Course } from "./courses.interfaces";
import { BackFeaturedCourse } from "./responses/featured.response";
import { Item } from "./responses/normal.response";

/**
 * Map Api Featured Course Response to App Course
 */
export function fromFeatured(back: BackFeaturedCourse): Course {
  let delivery = back.coursePublication.course.deliveryMethod;
  return {
    id: back.coursePublication.course.id,
    name: back.coursePublication.course.name,
    featured: true,
    provider: back.coursePublication.course.provider.name,
    img: back.coursePublication.course.featuredBanner,
    hours: back.coursePublication.totalHours,
    price: back.coursePublication.price,
    delivery: delivery && delivery.name
  };
}

export function fromNormal(item: Item): Course {
  return {
    id: item.course.id,
    name: item.course.name,
    featured: false,
    provider: item.course.provider.name,
    img: "",
    hours: item.course.components[0].profession.totalHours,
    price: item.price,
    delivery: item.course.deliveryMethod.description,
    isFree: item.isFree
  };
}
