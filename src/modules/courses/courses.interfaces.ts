export interface Course {
  id: number;
  name: string;
  price: number;
  hours: number;
  provider: string;
  delivery: string;
  img: string;
  featured: boolean;
  isFree?: boolean;
}
