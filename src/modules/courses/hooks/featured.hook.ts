import { useState } from "react";
import { Course } from "../courses.interfaces";
import { courses } from "../courses";

export function useFeatured() {
  let [featured, setFeatured] = useState<Course[]>([]);

  const fetch = async () => {
    setFeatured(await courses.getFeatured());
  };

  return { courses: featured, fetch };
}
