import { Course } from "../courses.interfaces";
import { useState } from "react";
import { courses } from "../courses";
import { memoize, debounce } from "lodash";

interface Result {
  items: Course[];
  total: number;
}

/**
 * Make getNormals Results Cached
 */
const getNormals = memoize(courses.getNormals, (show, page, search) => {
  return JSON.stringify({ show, page, search });
});

export function useCourses(show: number) {
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [result, setResult] = useState<Result>({ items: [], total: 0 });
  const [query, setQuery] = useState("");
  const [size, setSize] = useState(show);

  const fetch = async () => {
    setLoading(true);
    setResult(await getNormals(size, page, query));
    setLoading(false);
  };

  return {
    result,
    fetch,
    loading,
    setPage,
    page,
    query,
    setQuery: debounce(setQuery, 1000),
    size,
    setSize
  };
}
