import axios from "axios";
import { BackFeaturedCourse } from "./responses/featured.response";
import { fromFeatured, fromNormal } from "./courses.mappers";
import { NormalCourseResponse } from "./responses/normal.response";

const api = {
  featured: "https://api.cebroker.com/v2/featuredCoursesProfession",
  courses: "https://api.cebroker.com/v2/search/courses"
};

/**
 * Get Featured Courses
 */
async function getFeatured() {
  let res = await axios.get<BackFeaturedCourse[]>(api.featured, {
    params: { profession: 36 }
  });
  return res.data.map(fromFeatured);
}

/**
 * Get Normal Courses
 */
async function getNormals(show: number, page: number, search?: string) {
  let params = {
    profession: 36,
    courseType: "CD_ANYTIME",
    sortShufflingSeed: 27,
    expand: "totalItems",
    pageIndex: page,
    pageSize: show,
    sortField: "RELEVANCE",
    courseName: search
  };

  let res = await axios.get<NormalCourseResponse>(api.courses, { params });

  return {
    items: res.data.items.map(fromNormal),
    total: res.data.totalItems
  };
}

export const courses = { getFeatured, getNormals };
