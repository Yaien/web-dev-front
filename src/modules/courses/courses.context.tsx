import React from "react";
import { useCourses } from "./hooks/courses.hook";
import { useFeatured } from "./hooks/featured.hook";

export interface CoursesContext {
  normals: ReturnType<typeof useCourses>;
  featured: ReturnType<typeof useFeatured>;
}

export const CoursesContext = React.createContext<CoursesContext>(null as any);

export function QueryCourses(props: React.Props<{}>) {
  let normals = useCourses(5);
  let featured = useFeatured();

  let value = { normals, featured };
  return (
    <CoursesContext.Provider value={value}>
      {props.children}
    </CoursesContext.Provider>
  );
}
