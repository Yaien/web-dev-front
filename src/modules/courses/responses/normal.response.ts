export interface DeliveryMethod {
  code: string;
  description: string;
}

export interface MailAddres {
  streetAddress: string;
  streetAddress2: string;
  city: string;
  country: string;
  zipCode: string;
  state: string;
}

export interface Provider {
  id: number;
  hasLimitedRosterNotifications: boolean;
  hasLimitedRosterEmails: boolean;
  hasShowButtonNewCourse: boolean;
  hasShowButtonAdoptCourse: boolean;
  rosterCompletionNotificationsEnabled: boolean;
  rosterCompletionEmailsEnabled: boolean;
  rosterRejectionNotificationsEnabled: boolean;
  rosterRejectionEmailsEnabled: boolean;
  quarterlyReportEnabled: boolean;
  name: string;
  biography?: any;
  website: string;
  primaryContactId: number;
  previousNames?: any;
  isInHouse: boolean;
  isCourseNumberRequired: boolean;
  mailAddress: MailAddres;
  businessAddress?: any;
  phone: string;
  phoneExtension?: any;
  fax?: any;
  tollFree?: any;
  registrationPhone?: any;
  registrationWebsite?: any;
  logoToken?: any;
  logoUrl: string;
  hasEnrollmentNumber: boolean;
  defaultStateId: number;
  isNotOfferingCourse: boolean;
  isAllowedCourseUpload: boolean;
  instructorCreationEnabled: boolean;
  averageReportingTime?: any;
  isNotSingleStatus: boolean;
  isStripeEnabled: boolean;
}

export interface Board {
  id: number;
  name: string;
  code?: any;
  state?: any;
  mailAddress?: any;
  businessAddress?: any;
  boardFaqUrl?: any;
  isCustomProviderRequired: boolean;
  minHoursIncrement: number;
  hoursPrecision: string;
  isAllowedConcurrentSession: boolean;
  section?: any;
  hasSpecialFee: boolean;
  isCheckPaymentDenied: boolean;
  isCreditCardPaymentDenied: boolean;
  hoursPrecisionValue: number;
  applicationsSettings?: any;
  subTypes?: any;
  hasSubType: boolean;
  boardDeliveryMethodSettings?: any;
  professions?: any;
  routings?: any;
  hasNotShowCourseAddition: boolean;
  amRenewalDaysWindow?: any;
  hasCustomPayment: boolean;
}

export interface SubjectArea {
  id: number;
  name: string;
  code?: any;
  description?: any;
  minHours: number;
  maxHoursForLiveCourse: number;
  maxHoursForAnyTimeCourse: number;
  hasToApplyMaxHours: boolean;
  hasToApplyMinHours: boolean;
  requestedHours: number;
  approvedBoards?: any;
  approvedBoardHours?: any;
  approvedHours: number;
  profession?: any;
  message: string;
  xref: number;
}

export interface Profession {
  score: number;
  id: number;
  name: string;
  code?: any;
  boardId: number;
  board: Board;
  allowCEPackages: boolean;
  totalHours: number;
  subjectAreas: SubjectArea[];
  inNotTrackedByCebroker: boolean;
  inMandatoryCeAtCompliance: boolean;
  indohTrascriptView: boolean;
  inProfessionCourseRoute: boolean;
  isAvailableInCourseSearch: boolean;
  regularExpression?: any;
  courseSearchAlertMessage?: any;
  minHoursIncrement: number;
  hoursPrecision: string;
  specialties?: any;
}

export interface Component {
  courseId: number;
  subjectAreaId: number;
  professionId: number;
  submissionId: number;
  requestedHours: number;
  approvedHours: number;
  profession: Profession;
  subjectArea?: any;
}

export interface Course {
  id: number;
  name: string;
  type: string;
  featuredBanner: string;
  deliveryMethod: DeliveryMethod;
  provider: Provider;
  components: Component[];
  courseSubType?: any;
  inSeries: boolean;
  inModular: boolean;
  inConcurredSession: boolean;
  popularity: number;
  dateAdded: string;
  subjectAreaMessage?: any;
  registrationWebsite: string;
  isSaved: boolean;
}

export interface Location {
  id: number;
  submitterId: number;
  providerId: number;
  name: string;
  address: string;
  additionalAddress?: any;
  building?: any;
  buildingNumber?: any;
  room?: any;
  phone?: any;
  zipCode: string;
  state: string;
  city: string;
  country?: any;
  street?: any;
  status: boolean;
  stateName: string;
  countryName?: any;
  number?: any;
  enrollmentMaxNumber: number;
  comments?: any;
}

export interface Item {
  id: number;
  startDate: string;
  endDate: string;
  price: number;
  isFree: boolean;
  totalHours: number;
  hoursBySubjectArea: number;
  course: Course;
  location: Location;
  hasPrice: boolean;
}

export interface NormalCourseResponse {
  items: Item[];
  totalItems: 58001;
}
