import React from "react";
import { Header } from "./components/Header";
import { Search } from "./components/Search";
import { Content } from "./components/Content";
import { QueryCourses } from "./modules/courses/courses.context";

const App = () => {
  return (
    <QueryCourses>
      <Header />
      <Search />
      <Content />
    </QueryCourses>
  );
};

export default App;
