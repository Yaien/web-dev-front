import React from "react";
import { Button } from "reactstrap";
import { Course } from "../modules/courses";

export interface CourseProps {
  course: Course;
}

export function CourseCard({ course }: CourseProps) {
  let featured = course.featured && <Featured />;
  return (
    <div className="course">
      <Picture img={course.img} />
      <div className="course-info">
        <div className="course-name">{course.name}</div>
        {featured}
        <div className="course-place">{course.provider}</div>
        <div className="d-flex flex-wrap">
          <Tag icon="schedule">{course.hours} Hours</Tag>
          <Tag icon="computer">{course.delivery}</Tag>
        </div>
      </div>
      <div className="d-flex flex-column justify-content-between">
        <Price value={course.price} isFree={course.isFree} />
        <Button outline className="d-flex justify-content-center">
          <i className="material-icons">chevron_right</i>
        </Button>
      </div>
    </div>
  );
}

export const Featured = () => <span className="course-badge">FEATURED</span>;

export interface TagProps extends React.Props<{}> {
  icon: string;
}

export function Tag(props: TagProps) {
  if (!props.children) return null;
  return (
    <p className="course-tag pr-2">
      <i className="material-icons course-tag-icon">{props.icon}</i>
      {props.children}
    </p>
  );
}

export interface PriceProps {
  value: number;
  isFree?: boolean;
}

export function Price(props: PriceProps) {
  let price = null;
  if (props.value) {
    price = "$" + props.value;
  }
  if (props.isFree) {
    price = "Free";
  }
  return <div className="course-price">{price}</div>;
}

export interface PictureProps {
  img: string;
}

export function Picture(props: PictureProps) {
  if (!props.img) return null;
  let img = "https://storage.cebroker.com/CEBroker/" + props.img;
  return <img className="img-fluid course-thumbnail" src={img} alt="" />;
}
