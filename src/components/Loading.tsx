import React from "react";
import { Spinner } from "reactstrap";

export function Loading() {
  return (
    <div className="h-100 w-100 d-flex justify-content-center m-5">
      <Spinner color="secondary" data-testid="loading" />
    </div>
  );
}
