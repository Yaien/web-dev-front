import React from "react";
import { Card, CardBody, FormGroup, Input, Label } from "reactstrap";

export interface FilterTitleProps {
  title: string;
}

export function FilterTitle(props: FilterTitleProps) {
  return (
    <div className="d-flex justify-content-between">
      <div className="filter-title">{props.title}</div>
      <i className="material-icons float-right">expand_less</i>
    </div>
  );
}

export interface RadioProps {
  label: string;
  name: string;
}

export function Radio(props: RadioProps) {
  return (
    <FormGroup check>
      <Label check className="d-flex">
        <Input type="radio" name={props.name} />
        <span className="ml-2">{props.label}</span>
      </Label>
    </FormGroup>
  );
}

export const FilterBody = (props: React.Props<{}>) => (
  <div className="mt-2">{props.children}</div>
);

export const Title = (props: React.Props<{}>) => (
  <>
    <i className="material-icons float-left">expand_more</i>
    <div className="filter-title">{props.children}</div>
  </>
);

export function Filters() {
  return (
    <Card>
      <CardBody>
        <Title>FILTER COURSE RESULTS</Title>
      </CardBody>
      <CardBody>
        <FilterTitle title="Course type" />
        <FilterBody>
          <Radio name="course" label="Self Paced" />
          <Radio name="course" label="Live" />
        </FilterBody>
      </CardBody>
      <CardBody>
        <FilterTitle title="Delivery type" />
        <FilterBody>
          <Radio name="delivery" label="Any delivery type" />
          <Radio name="delivery" label="Computer Based Training" />
          <Radio name="delivery" label="Correspondence" />
          <Radio name="delivery" label="Mailed Material" />
        </FilterBody>
      </CardBody>
      <CardBody>
        <FilterTitle title="Subject Area" />
        <FilterBody>
          <Radio name="subject" label="Any Subject Area" />
          <Radio name="subject" label="HIV/AIDS" />
          <Radio
            name="subject"
            label="End-of-Life Care and Palliative Health Care"
          />
          <Radio name="subject" label="Domestic Violence" />
        </FilterBody>
      </CardBody>
    </Card>
  );
}
