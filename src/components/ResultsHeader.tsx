import React, { useState } from "react";
import {
  CardTitle,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

export interface ResultsHeaderProps {
  page: number;
  results: number;
}

export function ResultsHeader(props: ResultsHeaderProps) {
  let [open, setOpen] = useState(false);
  let toggle = () => setOpen(!open);
  return (
    <CardTitle className="d-flex justify-content-between align-items-center">
      <span>
        Page {props.page} of {props.results} Results
      </span>
      <div>
        <span className="mr-2">Sorted by: </span>
        <ButtonDropdown size="sm" isOpen={open} toggle={toggle}>
          <DropdownToggle outline caret>
            Relevance
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem>Relevance</DropdownItem>
            <DropdownItem>Popularity</DropdownItem>
            <DropdownItem>CE Hours</DropdownItem>
            <DropdownItem>Name</DropdownItem>
            <DropdownItem>Price</DropdownItem>
          </DropdownMenu>
        </ButtonDropdown>
      </div>
    </CardTitle>
  );
}
