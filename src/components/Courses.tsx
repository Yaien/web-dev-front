import React from "react";
import { Course } from "../modules/courses";
import { CourseCard } from "./CourseCard";
import { Loading } from "./Loading";

export interface CoursesProps extends React.Props<{}> {
  courses: Course[];
  loading?: boolean;
}

export function Courses(props: CoursesProps) {
  if (props.loading) return <Loading />;
  return (
    <>
      {props.courses.map(course => (
        <CourseCard key={course.id} course={course} />
      ))}
      {props.children}
    </>
  );
}
