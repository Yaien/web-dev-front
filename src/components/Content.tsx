import React from "react";
import { Container, Row, Col } from "reactstrap";
import { Filters } from "./Filters";
import { Results } from "./Results";

export function Content() {
  return (
    <Container className="pt-5">
      <Row>
        <Col md={3}>
          <Filters />
        </Col>
        <Col md={9}>
          <Results />
        </Col>
      </Row>
    </Container>
  );
}
