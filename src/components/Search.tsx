import React, { useContext } from "react";
import {
  Container,
  Button,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  NavItem,
  NavLink,
  Nav
} from "reactstrap";
import { CoursesContext } from "../modules/courses";

export function Search() {
  let { normals } = useContext(CoursesContext);

  const change = (e: React.ChangeEvent<HTMLInputElement>) => {
    normals.setQuery(e.target.value);
  };

  return (
    <Container
      fluid
      className="search-bg d-flex flex-column align-items-center justify-content-center pt-3 pb-0"
    >
      <div className="d-flex align-items-center">
        <div className="search-text">Find CE for</div>
        <Button outline color="light" className="search-text">
          Florida
        </Button>
        <Button outline color="light" className="search-text">
          Medical Doctor
        </Button>
      </div>
      <div className="m-3 w-50">
        <InputGroup size="lg">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="material-icons">search</i>
            </InputGroupText>
          </InputGroupAddon>
          <Input name="search" onChange={change} />
        </InputGroup>
      </div>
      <div>
        <Nav>
          <NavItem>
            <NavLink active className="text-white">
              COURSES
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className="text-white">PROVIDERS</NavLink>
          </NavItem>
        </Nav>
      </div>
    </Container>
  );
}
