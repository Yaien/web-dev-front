import React, { useState } from "react";
import {
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button
} from "reactstrap";

export function Header() {
  const [open, setOpen] = useState(false);
  const toggle = () => setOpen(!open);
  return (
    <Navbar color="light" light expand="md" className="shadow sticky-top">
      <NavbarBrand href="/">
        <img
          src="https://clubdemocanada.com/wp-content/uploads/sites/16/2016/06/special-events-icon-biggie.png"
          width="60"
          height="60"
          className="d-inline-block align-center mx-2"
          alt=""
        />
      </NavbarBrand>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={open} navbar>
        <Nav navbar>
          <NavItem>
            <NavLink>Features</NavLink>
          </NavItem>
          <NavItem>
            <NavLink>Plans</NavLink>
          </NavItem>
          <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
              Organizations
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem>Option 1</DropdownItem>
              <DropdownItem>Option 2</DropdownItem>
              <DropdownItem divider />
              <DropdownItem>Reset</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
          <NavItem>
            <NavLink active>Browse Courses</NavLink>
          </NavItem>
          <NavItem>
            <NavLink>Support</NavLink>
          </NavItem>
        </Nav>
      </Collapse>
      <Nav>
        <NavItem>
          <Button outline className="m-1">
            Sign in
          </Button>
          <Button color="success" className="m-1">
            7 day trial
          </Button>
        </NavItem>
        <NavItem />
      </Nav>
    </Navbar>
  );
}
