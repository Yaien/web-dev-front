import React from "react";
import { Courses } from "../Courses";
import { render, cleanup } from "react-testing-library";

describe("Courses", () => {
  afterEach(cleanup);

  it("Should display loading when loading prop is true", () => {
    let { container, queryByTestId } = render(<Courses courses={[]} loading />);
    expect(queryByTestId("loading")).toBeTruthy();
    expect(container.querySelector(".course")).toBeFalsy();
  });

  it("Should display courses", () => {
    let courses: any[] = [{ id: 1, name: "x" }, { id: 2, name: "y" }];
    let { container, queryByTestId } = render(<Courses courses={courses} />);
    expect(queryByTestId("loading")).toBeFalsy();
    expect(container.querySelectorAll(".course")).toHaveLength(courses.length);
  });
});
