import React from "react";
import { render, cleanup } from "react-testing-library";
import { CoursesContext } from "../../modules/courses";
import { FeaturedCourses } from "../FeaturedCourses";
import { CourseCard } from "../CourseCard";

describe("FeaturedCourse", () => {
  let value = {
    featured: {
      fetch: jest.fn(),
      courses: [
        { id: 1, name: "Course X" },
        { id: 2, name: "Course Y" },
        { id: 3, name: "Course Z" }
      ]
    }
  };

  afterEach(cleanup);

  it("Should fetch and display featured courses", () => {
    let component = render(
      <CoursesContext.Provider value={value as any}>
        <FeaturedCourses />
      </CoursesContext.Provider>
    );

    expect(value.featured.fetch).toHaveBeenCalled();
    expect(component.asFragment().querySelectorAll(".course")).toHaveLength(
      value.featured.courses.length
    );
  });
});
