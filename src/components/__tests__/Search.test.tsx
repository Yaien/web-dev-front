import React from "react";
import { render, fireEvent } from "react-testing-library";
import { CoursesContext } from "../../modules/courses";
import { Search } from "../Search";

describe("Search", () => {
  let value = {
    normals: {
      query: "",
      setQuery: function(query: string) {
        this.query = query;
      }
    }
  };

  it("Should set context query on change", () => {
    let { container } = render(
      <CoursesContext.Provider value={value as any}>
        <Search />
      </CoursesContext.Provider>
    );

    let input = container.querySelector("input[name='search']") as Element;
    let search = "search";
    fireEvent.change(input, { target: { value: search } });
    expect(value.normals.query).toBe(search);
  });
});
