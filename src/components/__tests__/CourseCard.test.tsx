import React from "react";
import { render, cleanup } from "react-testing-library";
import { CourseCard, Price } from "../CourseCard";
import { Course } from "../../modules/courses";

describe("CourseCard", () => {
  afterEach(cleanup);

  it("Price should render 'Free' when isFree set to true", () => {
    let component = render(<Price value={0} isFree />);
    expect(component.getByText("Free")).toBeTruthy();
  });

  it("Price should render value", () => {
    let component = render(<Price value={10} />);
    expect(component.getByText("$10")).toBeTruthy();
  });

  it("Price shouldn't render value if is falsy", () => {
    let component = render(<Price value={0} isFree />);
    expect(() => component.getByText("$")).toThrow();
  });

  it("Img should not render if there's no source", () => {
    let course: any = { img: null };
    let component = render(<CourseCard course={course} />);
    expect(component.baseElement.querySelector("img")).toBeFalsy();
  });

  it("Should display 'FEATURED' when is a featured course", () => {
    let course: any = { featured: true };
    let component = render(<CourseCard course={course} />);
    expect(
      component.getByText("FEATURED", { selector: "span.course-badge" })
    ).toBeTruthy();
  });
});
