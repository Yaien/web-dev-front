import React, { useEffect, useContext } from "react";
import { CoursesContext } from "../modules/courses";
import { CourseCard } from "./CourseCard";

export function FeaturedCourses() {
  let { featured } = useContext(CoursesContext);

  useEffect(() => {
    featured.fetch();
  }, []);

  return (
    <>
      {featured.courses.map(course => (
        <CourseCard key={course.id} course={course} />
      ))}
    </>
  );
}
