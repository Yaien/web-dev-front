import Pagination from "react-js-pagination";
import React, { useContext, useEffect } from "react";
import { Card } from "reactstrap";
import { Courses } from "./Courses";
import { FeaturedCourses } from "./FeaturedCourses";
import { CoursesContext } from "../modules/courses";
import { ResultsHeader } from "./ResultsHeader";

export function Results() {
  let { normals } = useContext(CoursesContext);
  let { page, fetch, query, result, setPage, size, loading } = normals;
  let { items, total } = result;

  useEffect(() => {
    setPage(1);
    fetch();
  }, [query]);

  useEffect(() => {
    fetch();
  }, [page]);

  return (
    <Card body>
      <ResultsHeader page={page} results={total} />
      <FeaturedCourses />
      <Courses courses={items} loading={loading}>
        <div className="m-auto pt-2">
          <Pagination
            activePage={page}
            itemsCountPerPage={size}
            totalItemsCount={total}
            pageRangeDisplayed={5}
            onChange={setPage}
            itemClass="page-item"
            linkClass="page-link"
          />
        </div>
      </Courses>
    </Card>
  );
}
